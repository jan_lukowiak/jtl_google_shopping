<?php /** @noinspection MissingIssetImplementationInspection */
declare(strict_types=1);

namespace Plugin\jtl_google_shopping\src\Exportformat;

use Exception;
use Illuminate\Support\Collection;
use JTL\Catalog\Category\Kategorie;
use JTL\Catalog\Product\Artikel;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Extensions\Config\Configurator;
use JTL\Helpers\ShippingMethod;
use JTL\Helpers\Tax;
use JTL\Helpers\Text;
use JTL\Plugin\Data\Localization;
use JTL\Shop;
use Psr\Log\LoggerInterface;
use Session;

/**
 * Class GoogleShoppingXML
 * Exportiert Artikel in Form einer XML-Datei für Google Base
 * @package Plugin\jtl_google_shopping\src\Exportformat
 */
final class GoogleShoppingXML
{
    /** @var DbInterface */
    private $db;

    /** @var LoggerInterface */
    private $logger;

    /** @var object */
    private $exportformat;

    /** @var Resource */
    private $f;

    /** @var Localization */
    private $localization;

    /** @var string */
    private $header = /** @lang text */
        '<?xml version="1.0"?>' . "\r"
        . '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">' . "\r"
        . "\t" . '<channel>' . "\r"
        . "\t\t" . '<title><![CDATA[###cShop###]]></title>' . "\r"
        . "\t\t" . '<link><![CDATA[###cShopUrl###]]></link>' . "\r"
        . "\t\t" . '<description><![CDATA[###cShopBeschreibung###]]></description>' . "\r";

    /** @var string */
    private $footer = "\t</channel>\r</rss>";

    /** @var Collection */
    private $settings;

    /** @var bool */
    private $headInitialized = false;

    /** @var bool */
    private $headWritten = false;

    /** @var array */
    private $exportProductIDs;

    /** @var Artikel[] */
    private $exportProducts = [];

    /** @var array */
    private $attributes = [];

    /** @var array */
    private $unitCodeMapping = [
        '[lb_av]' => 'lb',
        'm2'      => 'sqm',
        'm3'      => 'cbm',
        '[in_i]'  => 'in',
    ];

    /** @var array */
    private $unitCodes = [
        'oz', 'lb', 'mg', 'g', 'kg',
        'floz', 'pt', 'qt', 'gal', 'ml', 'cl', 'l', 'cbm',
        'in', 'ft', 'yd', 'cm', 'm',
        'sqft', 'sqm', 'ct',
    ];

    /**
     * Konstruktor
     * setzt $exportformat, $f und $cEinstellung_arr
     *
     * @param object      $exportformat
     * @param mixed       $f
     * @param Collection  $settings
     * @param DbInterface $db
     * @throws Exception
     */
    public function __construct(object $exportformat, $f, Collection $settings, DbInterface $db)
    {
        $this->db     = $db;
        $this->logger = Shop::Container()->getLogService();

        if (isset($exportformat) && \is_object($exportformat)) {
            $this->exportformat = $exportformat;
        } else {
            throw new Exception(\__('An error occurred while loading the export format'));
        }

        if (isset($f) && \is_resource($f)) {
            $this->f = $f;
        } else {
            throw new Exception(\__('An error occurred while getting the file handle'));
        }

        if (isset($settings)) {
            $this->settings = $settings;
        } else {
            throw new Exception(\__('An error occurred while loading the settings'));
        }

        $this->loadAttr()
             ->initHead();
    }

    /**
     * @param string|null $code
     * @return string
     */
    private function mapUnitCode(?string $code): string
    {
        if (empty($code)) {
            return '';
        }
        $code = $this->unitCodeMapping[$code] ?? \mb_strtolower($code);

        return \in_array($code, $this->unitCodes, true) ? $code : '';
    }

    /**
     * Lädt optionale Attribute die der Benutzer in der Plugin-Einstellung selber definiert aus der DB
     *
     * @return self
     */
    private function loadAttr(): self
    {
        $attributes = $this->db->query(/** @lang text */
            'SELECT kAttribut, kVaterAttribut, cGoogleName, cWertName, eWertHerkunft
                FROM xplugin_jtl_google_shopping_attribut
                WHERE bAktiv = 1
                ORDER BY kVaterAttribut ASC',
            ReturnType::ARRAY_OF_OBJECTS
        );
        foreach ($attributes as $attribute) {
            $parent = $attribute->kVaterAttribut;
            if ($parent > 0) {
                if (isset($this->attributes[$parent]->oKindAttr_arr)
                    && \count($this->attributes[$parent]->oKindAttr_arr) > 0
                ) {
                    $this->attributes[$parent]->oKindAttr_arr[$attribute->kAttribut] = $attribute;
                } else {
                    $this->attributes[$parent]->oKindAttr_arr = [$attribute->kAttribut => $attribute];
                }
            } else {
                $this->attributes[$attribute->kAttribut] = $attribute;
            }
        }

        return $this;
    }

    /**
     * Initialisiert den Head (ersetzt Platzhalter für Shopname, -beschreibung und -URL)
     *
     * @return self
     */
    public function initHead(): self
    {
        $this->header = \str_replace(
            '###cShop###',
            Text::htmlentities($this->settings->get('shopname')),
            $this->header
        );
        $this->header = \str_replace('###cShopUrl###', URL_SHOP, $this->header);
        $this->header = \str_replace(
            '###cShopBeschreibung###',
            Text::htmlentities($this->settings->get('shopbeschreibung')),
            $this->header
        );

        $this->headInitialized = true;

        return $this;
    }

    /**
     * Schreibt $cHead in die Datei $f
     *
     * @return self
     */
    public function writeHead(): self
    {
        if ($this->headInitialized && !$this->headWritten) {
            \fwrite($this->f, $this->header);
            $this->headWritten = true;
        }

        return $this;
    }

    /**
     * Setzt die zu exportierenden kArtikel
     *
     * @param array $exportProducts
     * @return self
     */
    public function setExportProductIds(array $exportProducts): self
    {
        if ($this->exportProductIDs === null && isset($exportProducts) && \is_array($exportProducts)) {
            $this->exportProductIDs = [];
            foreach ($exportProducts as $product) {
                $this->exportProductIDs[] = (int)$product['kArtikel'];
            }
        }

        return $this;
    }

    /**
     * @param Localization $localization
     * @return self
     */
    public function setLocalization(Localization $localization): self
    {
        $this->localization = $localization;

        return $this;
    }

    /**
     * Lädt Artikelobjekte
     *
     * @param int $productID
     * @return self
     * @throws Exception
     */
    public function loadExportProduct(int $productID = 0): self
    {
        if ($productID > 0) {
            $this->exportProducts[$productID] = new Artikel();
            try {
                $this->exportProducts[$productID]->fuelleArtikel(
                    $productID,
                    Artikel::getExportOptions(),
                    $this->exportformat->kKundengruppe,
                    $this->exportformat->kSprache,
                    true
                );
            } catch (Exception $e) {
                unset($this->exportProducts[$productID]);

                return $this;
            }
            if ($this->exportProducts[$productID]->kArtikel === null) {
                unset($this->exportProducts[$productID]);
                $this->logger->notice(
                    \sprintf(
                        \__('Product could not be exported because no product exists for current settings'),
                        $productID
                    )
                );

                return $this;
            }
            // Kampagne URL
            $cSep = '?';
            if (isset($this->exportProducts[$productID]->cDeeplink)
                && \mb_strpos($this->exportProducts[$productID]->cDeeplink, '.php')
            ) {
                $cSep = '&';
            }
            $this->exportProducts[$productID]->cDeeplink = \rtrim(URL_SHOP, ' \/') . '/'
                . $this->exportProducts[$productID]->cURL . $cSep . 'curr=' . Session::getCurrency()->getCode();

            $cSep = '&';

            if (isset($this->exportformat->tkampagne_cParameter)) {
                $this->exportProducts[$productID]->cDeeplink .= $cSep
                    . $this->exportformat->tkampagne_cParameter . '=' . $this->exportformat->tkampagne_cWert;
            }

            if (isset($this->exportProducts[$productID]->kStueckliste)
                && $this->exportProducts[$productID]->kStueckliste > 0
            ) {
                $this->exportProducts[$productID]->bIsBundle = 'TRUE';
            }
            $this->exportProducts[$productID]->fUst      = Tax::getSalesTax(
                $this->exportProducts[$productID]->kSteuerklasse
            );
            $this->exportProducts[$productID]->fVKBrutto = Tax::getGross(
                $this->exportProducts[$productID]->Preise->fVKNetto * Session::getCurrency()->getConversionFactor(),
                $this->exportProducts[$productID]->fUst
            ) . ' ' . Session::getCurrency()->getCode();

            if ((int)$this->exportProducts[$productID]->nIstVater === 0
                && $this->exportProducts[$productID]->kVaterArtikel > 0
            ) {
                $this->loadExportProduct($this->exportProducts[$productID]->kVaterArtikel);
                if (isset($this->exportProducts[$this->exportProducts[$productID]->kVaterArtikel]->kArtikel)) {
                    if ((int)$this->settings->get('ext_artnr_child') === 1) {
                        $this->exportProducts[$productID]->cArtNr .= '_' . $this->exportProducts[$productID]->kArtikel;
                    }
                    $this->exportProducts[$productID]->cVaterArtNr = $this->exportProducts[
                                                                     $this->exportProducts[$productID]->kVaterArtikel
                    ]->cArtNr;
                    unset($this->exportProducts[$this->exportProducts[$productID]->kVaterArtikel]);
                } else {
                    unset(
                        $this->exportProducts[$productID],
                        $this->exportProducts[$this->exportProducts[$productID]->kVaterArtikel]
                    );
                    $this->logger->notice(
                        \sprintf(\__('Product could not be exported because no parent product exists'), $productID)
                    );

                    return $this;
                }
            }

            $this->loadProductAttributes($productID)
                 ->loadAvailibility($productID)
                 ->loadImages($productID)
                 ->loadGtin($productID)
                 ->loadState($productID)
                 ->loadShipping($productID)
                 ->loadCategory($productID)
                 ->loadGoogleCategory($productID)
                 ->loadSale($productID)
                 ->loadUnitPricing($productID)
                 ->loadConfigPrice($productID)
                 ->formatItems($productID);
        }

        return $this;
    }

    /**
     * Lädt Artikelmerkmale (Größe, Farbe, ...) in das zugehörige Artikelobjekt
     *
     * @param int $productID
     * @return self
     */
    private function loadProductAttributes(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        if (isset($this->exportProducts[$productID]->cMerkmalAssoc_arr)
            && \is_array($this->exportProducts[$productID]->cMerkmalAssoc_arr)
        ) {
            $mappings              = $this->db->query(/** @lang text */
                'SELECT cVon, cZu, cType
                    FROM xplugin_jtl_google_shopping_mapping',
                ReturnType::ARRAY_OF_OBJECTS
            );
            $mappedAttributes      = [];
            $mappedAttributeValues = [];
            foreach ($mappings as $mapping) {
                if (\mb_strtolower($mapping->cType) === 'merkmal') {
                    $mappedAttributes[$mapping->cVon] = $mapping->cZu;
                } elseif (\mb_strtolower($mapping->cType) === 'merkmalwert') {
                    $mappedAttributeValues[$mapping->cVon] = $mapping->cZu;
                }
            }

            foreach ($this->exportProducts[$productID]->oMerkmale_arr as $attribute) {
                $value = '';
                foreach ($attribute->oMerkmalWert_arr as $i => $attribValue) {
                    if ($i > 0) {
                        $value .= '/';
                    }
                    $value .= $attribValue->cWert ?? null;
                }
                if (\array_key_exists(\mb_strtolower($attribute->cName), $mappedAttributes)) {
                    $attribute->cName = $mappedAttributes[\mb_strtolower($attribute->cName)];
                }
                if (\array_key_exists(\mb_strtolower($value), $mappedAttributeValues)) {
                    $value = $mappedAttributeValues[\mb_strtolower($value)];
                }

                $lowerName = \mb_strtolower($attribute->cName);
                if (\str_replace(['ö', 'ß'], ['oe', 'ss'], $lowerName) === 'groesse') {
                    $this->exportProducts[$productID]->cGroesse = $value;
                } elseif ($lowerName === 'farbe') {
                    $this->exportProducts[$productID]->cFarbe = $value;
                } elseif ($lowerName === 'geschlecht') {
                    $this->exportProducts[$productID]->cGeschlecht = $value;
                } elseif ($lowerName === 'altersgruppe') {
                    $this->exportProducts[$productID]->cAltersgruppe = $value;
                } elseif ($lowerName === 'muster') {
                    $this->exportProducts[$productID]->cMuster = $value;
                } elseif ($lowerName === 'material') {
                    $this->exportProducts[$productID]->cMaterial = $value;
                }
            }
        }

        if ((int)$this->exportProducts[$productID]->nIstVater === 0
            && (int)$this->exportProducts[$productID]->kVaterArtikel > 0
        ) {
            foreach ($this->exportProducts[$productID]->Variationen as $variation) {
                if (\mb_strtolower($variation->cName) === 'farbe') {
                    foreach ($this->exportProducts[$productID]->oVariationenNurKind_arr as $variationProduct) {
                        if (\mb_strtolower($variationProduct->cName) === 'farbe') {
                            $this->exportProducts[$productID]->cFarbe = $variationProduct->Werte[0]->cName;
                        }
                    }
                } elseif (\mb_strtolower($variation->cName) === 'material') {
                    foreach ($this->exportProducts[$productID]->oVariationenNurKind_arr as $variationProduct) {
                        if (\mb_strtolower($variationProduct->cName) === 'material') {
                            $this->exportProducts[$productID]->cMaterial = $variationProduct->Werte[0]->cName;
                        }
                    }
                } elseif (\mb_strtolower($variation->cName) === 'muster') {
                    foreach ($this->exportProducts[$productID]->oVariationenNurKind_arr as $variationProduct) {
                        if (\mb_strtolower($variationProduct->cName) === 'muster') {
                            $this->exportProducts[$productID]->cMuster = $variationProduct->Werte[0]->cName;
                        }
                    }
                } elseif (\str_replace(['ö', 'ß'], ['oe', 'ss'], \mb_strtolower($variation->cName)) === 'groesse') {
                    foreach ($this->exportProducts[$productID]->oVariationenNurKind_arr as $variationProduct) {
                        if (\str_replace(
                            ['ö', 'ß'],
                            ['oe', 'ss'],
                            \mb_strtolower($variationProduct->cName)
                        ) === 'groesse') {
                            $this->exportProducts[$productID]->cGroesse = $variationProduct->Werte[0]->cName;
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Prüft die Verfügbarkeit und schreibt sie in das zugehörige Objekt
     *
     * @param int $productID
     * @return self
     */
    private function loadAvailibility(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        if ($this->exportProducts[$productID]->fLagerbestand > 0
            || $this->exportProducts[$productID]->cLagerBeachten === 'N'
            || $this->exportProducts[$productID]->cLagerKleinerNull === 'Y'
        ) {
            $this->exportProducts[$productID]->cVerfuegbarkeit = 'in stock';
        } else {
            $this->exportProducts[$productID]->cVerfuegbarkeit = 'out of stock';
        }

        return $this;
    }

    /**
     * Lädt die Bild-Links in das zugehörige Artikelobjekt
     *
     * @param int $productID
     * @return self
     */
    private function loadImages(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        $shopURL = Shop::getURL() . '/';

        $this->exportProducts[$productID]->Artikelbild = $shopURL
            . $this->exportProducts[$productID]->Bilder[0]->cPfadGross;

        $imageCount = \count($this->exportProducts[$productID]->Bilder);
        for ($i = 1; $i < $imageCount && $i <= 10; $i++) {
            $this->exportProducts[$productID]->cArtikelbild_arr[] = $shopURL
                . $this->exportProducts[$productID]->Bilder[$i]->cPfadGross;
        }

        return $this;
    }

    /**
     * Prüft, ob cBarcode oder cISBN gesetzt ist und lädt es als Gtin ins Artikelobjekt
     *
     * @param int $productID
     * @return self
     */
    private function loadGtin(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        if (isset($this->exportProducts[$productID]->cBarcode)
            && !empty($this->exportProducts[$productID]->cBarcode)
        ) {
            $this->exportProducts[$productID]->cGtin = $this->exportProducts[$productID]->cBarcode;
        } elseif (isset($this->exportProducts[$productID]->cISBN)
            && !empty($this->exportProducts[$productID]->cISBN)
        ) {
            $this->exportProducts[$productID]->cGtin = $this->exportProducts[$productID]->cISBN;
        }

        return $this;
    }

    /**
     * @param int $productID
     * @return self
     * @throws Exception
     */
    private function loadSale(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])
            || $this->exportProducts[$productID]->Preise->Sonderpreis_aktiv !== 1
        ) {
            return $this;
        }

        $currencyFactor = Session::getCurrency()->getConversionFactor();
        $currencyCode   = Session::getCurrency()->getCode();

        // Google: Das Attribut price [Preis] muss mit dem vollen (regulären) Preis für den Artikel
        // eingereicht werden
        $this->exportProducts[$productID]->salePrice = $this->exportProducts[$productID]->fVKBrutto;
        $this->exportProducts[$productID]->fVKBrutto =
            Tax::getGross(
                $this->exportProducts[$productID]->Preise->alterVKNetto * $currencyFactor,
                $this->exportProducts[$productID]->fUst
            ) . ' ' . $currencyCode;

        // Google: Wenn Sie das Attribut sale_price_effective_date [Sonderangebotszeitraum] nicht einreichen,
        // gilt immer das Attribut sale_price [Sonderangebotspreis].
        if (!empty($this->exportProducts[$productID]->Preise->SonderpreisBis_en)) {
            $this->exportProducts[$productID]->salePriceEffectiveDate =
                (new \DateTime())->sub(new \DateInterval('P1D'))->format('Y-m-d') . '/' .
                $this->exportProducts[$productID]->Preise->SonderpreisBis_en;
        }

        return $this;
    }

    /**
     * @param int $productID
     * @return self
     */
    private function loadUnitPricing(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        $unitCode     = $this->mapUnitCode($this->exportProducts[$productID]->cMasseinheitCode);
        $baseUnitCode = $this->mapUnitCode($this->exportProducts[$productID]->cGrundpreisEinheitCode);

        if (empty($unitCode) || empty($baseUnitCode)) {
            return $this;
        }

        if ($this->exportProducts[$productID]->fMassMenge > 0) {
            $this->exportProducts[$productID]->unitPricingMeasure =
                $this->exportProducts[$productID]->fMassMenge . ' ' . $unitCode;
        }
        if ($this->exportProducts[$productID]->fGrundpreisMenge > 0) {
            $this->exportProducts[$productID]->unitPricingBaseMeasure =
                $this->exportProducts[$productID]->fGrundpreisMenge . ' ' . $baseUnitCode;
        }

        return $this;
    }

    /**
     * Prüft ob für den Artikel ein Zustand verfügbar ist.
     * Wenn ja wird dieser geladen sonst Standart-Wert aus den Plugineinstellungen
     *
     * @param int $productID
     * @return self
     */
    private function loadState(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        if (isset($this->exportProducts[$productID]->FunktionsAttribute[$this->settings->get('artikle_condition')])
            && \is_string($this->exportProducts[$productID]->FunktionsAttribute[$this->settings->get(
                'artikle_condition'
            )])
        ) {
            $this->exportProducts[$productID]->cZustand =
                $this->exportProducts[$productID]->FunktionsAttribute[$this->settings->get('artikle_condition')];
        } else {
            $this->exportProducts[$productID]->cZustand = $this->settings->get('artikle_condition_standard');
        }

        return $this;
    }

    /**
     * Lädt Versanddaten (Versandklass, Lieferland und Versandkosten)
     *
     * @param int $productID
     * @return self
     */
    private function loadShipping(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        $this->exportProducts[$productID]->cLieferland = $this->settings->get('exportformate_lieferland');

        $fVersandkosten = \number_format((float)ShippingMethod::getLowestShippingFees(
            $this->settings->get('exportformate_lieferland'),
            $this->exportProducts[$productID],
            0,
            $this->exportformat->kKundengruppe
        ), 2);
        if ($fVersandkosten < 0) {
            $fVersandkosten = 0.00;
        }
        $this->exportProducts[$productID]->Versandkosten = $fVersandkosten . ' ' . Session::getCurrency()->getCode();

        return $this;
    }

    /**
     * Lädt Kategorie(n)
     *
     * @param int $productID
     * @return self
     */
    private function loadCategory(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        $count = 0;

        if (\count($this->exportProducts[$productID]->oKategorie_arr) > 0) {
            foreach ($this->exportProducts[$productID]->oKategorie_arr as $categoryID) {
                $category = new Kategorie(
                    $categoryID,
                    $this->exportformat->kSprache,
                    $this->exportformat->kKundengruppe
                );

                $this->exportProducts[$productID]->cCategorie_arr[] = \implode(' &gt; ', $category->cKategoriePfad_arr);
                if ($count++ >= 10) {
                    return $this;
                }
            }
        } else {
            unset($this->exportProducts[$productID]);
        }

        return $this;
    }

    /**
     * Lädt Google-Kategorie.
     * Wenn für den Artikel eine GoogleKategorie angegeben ist wird diese verwendet,
     * sonst die Google-Kategorie der Artikelkategorie im Shop
     *
     * @param int $productID
     * @return self
     */
    private function loadGoogleCategory(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        if (isset($this->exportProducts[$productID]->FunktionsAttribute[$this->settings->get('artikle_googlekat')])
            && \is_string($this->exportProducts[$productID]->FunktionsAttribute[$this->settings->get(
                'artikle_googlekat'
            )])
        ) {
            $this->exportProducts[$productID]->cGoogleCategorie[] = \str_replace(
                ['"', '>'],
                ['', '&gt;'],
                $this->exportProducts[$productID]->FunktionsAttribute[$this->settings->get('artikle_googlekat')]
            );
        } elseif (isset($this->exportProducts[$productID]->oKategorie_arr)
            && \is_array($this->exportProducts[$productID]->oKategorie_arr)
        ) {
            foreach ($this->exportProducts[$productID]->oKategorie_arr as $categoryID) {
                $category = new Kategorie(
                    $categoryID,
                    $this->exportformat->kSprache,
                    $this->exportformat->kKundengruppe
                );
                if (isset($category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')])
                    && !empty($category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')])
                ) {
                    if (empty($this->exportProducts[$productID]->cGoogleCategorie)
                        || !\is_array($this->exportProducts[$productID]->cGoogleCategorie)
                        || !\in_array(
                            \str_replace(
                                ['"', '>'],
                                ['', '&gt;'],
                                $category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')]
                            ),
                            $this->exportProducts[$productID]->cGoogleCategorie,
                            true
                        )
                    ) {
                        $this->exportProducts[$productID]->cGoogleCategorie[] = \str_replace(
                            ['"', '>'],
                            ['', '&gt;'],
                            $category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')]
                        );

                        return $this;
                    }
                } elseif ($category->kOberKategorie > 0) {
                    if ($this->loadGoogleCategoryVater($category->kOberKategorie, $productID)) {
                        return $this;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Lädt Google-Kategorie wenn keine beim Artikel hinterlegt ist oder bei der Kategorie keine angegeben ist.
     * Verwendet die Google-Kategorie der Vater-Kategorie im Shop
     *
     * @param int $categoryID
     * @param int $productID
     * @return bool
     */
    private function loadGoogleCategoryVater(int $categoryID, int $productID): bool
    {
        if (!isset($this->exportProducts[$productID])) {
            return false;
        }

        $category = new Kategorie($categoryID, $this->exportformat->kSprache, $this->exportformat->kKundengruppe);
        if (isset($category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')])
            && !empty($category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')])
        ) {
            if (!isset($this->exportProducts[$productID]->cGoogleCategorie)
                || !\is_array($this->exportProducts[$productID]->cGoogleCategorie)
                || !\in_array(
                    \str_replace(
                        ['"', '>'],
                        ['', '&gt;'],
                        $category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')]
                    ),
                    $this->exportProducts[$productID]->cGoogleCategorie,
                    true
                )
            ) {
                $this->exportProducts[$productID]->cGoogleCategorie[] = \str_replace(
                    ['"', '>'],
                    ['', '&gt;'],
                    $category->categoryFunctionAttributes[$this->settings->get('artikle_googlekat')]
                );

                return true;
            }
        } elseif ($category->kOberKategorie > 0) {
            if ($this->loadGoogleCategoryVater($category->kOberKategorie, $productID)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int $productID
     * @return self
     */
    private function loadConfigPrice(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])
            || !$this->exportProducts[$productID]->bHasKonfig
            || !Configurator::validateKonfig($productID)
        ) {
            return $this;
        }

        $configGroups = Configurator::getKonfig($productID, $this->exportformat->kSprache);
        $landingPrice = (float)$this->exportProducts[$productID]->fVKBrutto;

        foreach ($configGroups as $configGroup) {
            foreach ($configGroup->oItem_arr as $configItem) {
                if ($configItem->getSelektiert()) {
                    $configItem->fAnzahl = $configItem->getInitial();
                    $landingPrice       += $configItem->getFullPrice();
                }
            }
        }

        $this->exportProducts[$productID]->fVKBrutto = $landingPrice . ' ' . Session::getCurrency()->getCode();

        return $this;
    }

    /**
     * Formatiert die Artikelattribute für die Ausgabe als XML
     *
     * @param int $productID
     * @return self
     */
    private function formatItems(int $productID): self
    {
        if (!isset($this->exportProducts[$productID])) {
            return $this;
        }

        $this->exportProducts[$productID]->cBeschreibungHTML     = \str_replace(
            '"',
            '&quot;',
            $this->exportProducts[$productID]->cBeschreibung
        );
        $this->exportProducts[$productID]->cKurzBeschreibungHTML = \str_replace(
            '"',
            '&quot;',
            $this->exportProducts[$productID]->cKurzBeschreibung
        );

        $find    = ['<br />', '<br>', '</'];
        $replace = [' ', ' ', ' </'];

        //Wenn keine Beschreibung vorhanden ist dann nehme vordefinierten Text
        $defaultDesc = $this->localization !== null
            ? $this->localization->getTranslation('no_product_description_exists')
            : \__('Sorry, but there is no description for this product');
        if (empty($this->exportProducts[$productID]->cBeschreibung)) {
            $this->exportProducts[$productID]->cBeschreibung = $defaultDesc;
        }
        if (empty($this->exportProducts[$productID]->cKurzBeschreibung)) {
            $this->exportProducts[$productID]->cKurzBeschreibung = $defaultDesc;
        }

        $this->exportProducts[$productID]->cName = \mb_substr(\str_replace(
            $find,
            $replace,
            $this->exportProducts[$productID]->cName
        ), 0, 150);

        $this->exportProducts[$productID]->cName             = \strip_tags(
            $this->exportProducts[$productID]->cName
        );
        $this->exportProducts[$productID]->cBeschreibung     = \strip_tags(
            $this->exportProducts[$productID]->cBeschreibung
        );
        $this->exportProducts[$productID]->cKurzBeschreibung = \strip_tags(
            $this->exportProducts[$productID]->cKurzBeschreibung
        );

        $this->exportProducts[$productID]->cBeschreibung     = \mb_substr(\str_replace(
            $find,
            $replace,
            $this->exportProducts[$productID]->cBeschreibung
        ), 0, 5000);
        $this->exportProducts[$productID]->cKurzBeschreibung = \str_replace(
            $find,
            $replace,
            $this->exportProducts[$productID]->cKurzBeschreibung
        );

        return $this;
    }

    /**
     * Schreibt $cFoot in die Datei $f
     *
     * @return self
     */
    public function writeFooter(): self
    {
        \fwrite($this->f, $this->footer);

        return $this;
    }

    /**
     * Ruft für jeden Artikel die Methode writeArticle auf
     *
     * @return self
     * @throws Exception
     */
    public function writeContent(): self
    {
        if (!\is_array($this->exportProducts) || !\is_array($this->attributes)) {
            return $this;
        }

        foreach ($this->exportProductIDs as $productID) {
            $this->loadExportProduct($productID)
                 ->writeProduct($this->exportProducts[$productID] ?? new Artikel());

            unset($this->exportProducts[$productID]);
        }

        return $this;
    }

    /**
     * Schreibt Artikel in die Datei $f
     *
     * @param Artikel $product
     * @return self
     */
    private function writeProduct(Artikel $product): self
    {
        if ((int)$product->kArtikel <= 0 || \count($this->attributes) === 0) {
            return $this;
        }
        $prefixAttr  = "\t\t\t";
        $prefixChild = "\t\t\t\t";
        $xml         = "\t\t<item>\r";
        foreach ($this->attributes as $attribute) {
            if ($attribute->eWertHerkunft === 'VaterAttribut') {
                if (isset($attribute->oKindAttr_arr) && \count($attribute->oKindAttr_arr) > 0) {
                    $xml .= $prefixAttr . '<' . $attribute->cGoogleName . ">\r";
                    foreach ($attribute->oKindAttr_arr as $child) {
                        if ($child->eWertHerkunft === 'WertName') {
                            $xml .= $this->writeAttribute(
                                $prefixChild,
                                $child->cGoogleName,
                                $child->cWertName
                            );
                        } elseif ($child->eWertHerkunft === 'ArtikelEigenschaft') {
                            if (isset($product->{$child->cWertName}) && $product->{$child->cWertName} !== '') {
                                $xml .= $this->writeAttribute(
                                    $prefixChild,
                                    $child->cGoogleName,
                                    $product->{$child->cWertName}
                                );
                            }
                        } elseif ($child->eWertHerkunft === 'FunktionsAttribut') {
                            $idx = \mb_strtolower($child->cWertName);
                            if (!empty($product->FunktionsAttribute[$idx])) {
                                $xml .= $this->writeAttribute(
                                    $prefixChild,
                                    $child->cGoogleName,
                                    $product->FunktionsAttribute[$idx]
                                );
                            }
                        } elseif ($child->eWertHerkunft === 'Attribut') {
                            if (!empty($product->AttributeAssoc[$child->cWertName])) {
                                $xml .= $this->writeAttribute(
                                    $prefixChild,
                                    $child->cGoogleName,
                                    $product->AttributeAssoc[$child->cWertName]
                                );
                            }
                        } elseif ($child->eWertHerkunft === 'Merkmal') {
                            $valName = \preg_replace(
                                '/[^öäüÖÄÜßa-zA-Z0-9.\-_]/u',
                                '',
                                $child->cWertName
                            );
                            if (!empty($product->cMerkmalAssoc_arr[$valName])) {
                                $xml .= $this->writeAttribute(
                                    $prefixChild,
                                    $child->cGoogleName,
                                    $product->cMerkmalAssoc_arr[$valName]
                                );
                            }
                        }
                    }
                    $xml .= $prefixAttr . '</' . $attribute->cGoogleName . ">\r";
                }
            } elseif ($attribute->eWertHerkunft === 'WertName') {
                $xml .= $this->writeAttribute($prefixAttr, $attribute->cGoogleName, $attribute->cWertName);
            } elseif ($attribute->eWertHerkunft === 'ArtikelEigenschaft') {
                if (isset($product->{$attribute->cWertName}) && $product->{$attribute->cWertName} !== '') {
                    $xml .= $this->writeAttribute(
                        $prefixAttr,
                        $attribute->cGoogleName,
                        $product->{$attribute->cWertName}
                    );
                }
            } elseif ($attribute->eWertHerkunft === 'FunktionsAttribut') {
                $idx = \mb_strtolower($attribute->cWertName);
                if (!empty($product->FunktionsAttribute[$idx])) {
                    $xml .= $this->writeAttribute(
                        $prefixAttr,
                        $attribute->cGoogleName,
                        $product->FunktionsAttribute[$idx]
                    );
                }
            } elseif ($attribute->eWertHerkunft === 'Attribut') {
                if (!empty($product->AttributeAssoc[$attribute->cWertName])) {
                    $xml .= $this->writeAttribute(
                        $prefixAttr,
                        $attribute->cGoogleName,
                        $product->AttributeAssoc[$attribute->cWertName]
                    );
                }
            } elseif ($attribute->eWertHerkunft === 'Merkmal') {
                $valName = \preg_replace('/[^öäüÖÄÜßa-zA-Z0-9.\-_]/u', '', $attribute->cWertName);
                if (!empty($product->cMerkmalAssoc_arr[$valName])) {
                    $xml .= $this->writeAttribute(
                        $prefixAttr,
                        $attribute->cGoogleName,
                        $product->cMerkmalAssoc_arr[$valName]
                    );
                }
            }
        }
        $xml .= "\t\t</item>\r";
        \fwrite($this->f, $xml);

        return $this;
    }

    /**
     * Generiert einen XML-String für das Attribut: $cAttributeName mit dem Inhalt: $cContent
     * und dem Prefix: $cPreAttribute
     * Wenn $cContent ein Array ist dann ruft sich die Methode rekursiv auf
     *
     * @param string       $preAttribute
     * @param string       $attributeName
     * @param string|array $content
     * @return string mit XML für das Attribut
     */
    private function writeAttribute(string $preAttribute, string $attributeName, $content): string
    {
        $xml = '';
        if (isset($content) && \is_array($content)) {
            foreach ($content as $i => $value) {
                $xml .= $this->writeAttribute($preAttribute, $attributeName, $value);
                if ($attributeName === 'g:product_type' && $i === 9) {
                    break;
                }
            }
        } else {
            $xml .= $preAttribute . '<' . $attributeName . '><![CDATA['
                . \trim((string)$content)
                . ']]></' . $attributeName . ">\r";
        }

        return $xml;
    }
}
