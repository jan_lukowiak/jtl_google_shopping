<?php declare(strict_types=1);

namespace Plugin\jtl_google_shopping\src\Exportformat;

use DateTime;
use Exception;
use Illuminate\Support\Collection;
use JTL\Cron\QueueEntry;
use JTL\DB\ReturnType;
use JTL\Plugin\Helper;
use JTL\Plugin\PluginInterface;
use JTL\Shop;

/**
 * Class GoogleShoppingExport
 * @package Plugin\jtl_google_shopping\src\Exportformat
 */
final class GoogleShoppingExport
{
    /** @var PluginInterface */
    private $plugin;

    /** @var object */
    private $exportFormat;

    /** @var QueueEntry */
    private $queueEntry;

    /** @var string */
    private $tmpFilename;

    /** @var Collection */
    private $settings;

    /** @var resource */
    private $f;

    /** @var string */
    private $exportSQL;

    /** @var bool */
    private $isCron = false;

    /** @var bool */
    private $finished = false;

    private const EXPORT_PATH = \PFAD_ROOT . \PFAD_EXPORT;

    /**
     * GoogleShoppingExport constructor.
     * @param object $exportFormat
     * @param array  $settings
     */
    private function __construct(object $exportFormat, array $settings)
    {
        $this->exportFormat = $exportFormat;
        $this->settings     = new Collection();
        $this->plugin       = Helper::getLoaderByPluginID($exportFormat->kPlugin)->init(
            $exportFormat->kPlugin
        );
        $this->tmpFilename  = \mb_substr(
            $exportFormat->cDateiname,
            0,
            \mb_strrpos($exportFormat->cDateiname, '.')
        ) . '.xml';

        $this->init($settings);
    }

    /**
     * @param array $settings
     * @return void
     */
    private function init(array $settings): void
    {
        foreach ($this->plugin->getConfig()->getOptions() as $option) {
            $this->settings->put($option->valueID, $option->value);
        }
        foreach ($settings as $key => $value) {
            $this->settings->put($key, $value);
        }
    }

    /**
     * @return QueueEntry
     */
    public function getQueueEntry(): QueueEntry
    {
        return $this->queueEntry;
    }

    /**
     * @param QueueEntry $queueEntry
     * @param bool       $isCron
     * @return self
     */
    public function setQueueEntry(QueueEntry $queueEntry, bool $isCron): self
    {
        $this->queueEntry = $queueEntry;
        $this->isCron     = $isCron;

        if ($isCron) {
            $this->tmpFilename = 'cron_' . $this->tmpFilename;
        }

        $maxItems    = (int)$this->settings->get('maxItem');
        $doneItems   = $queueEntry->tasksExecuted + $queueEntry->taskLimit;
        $idxFilename = (int)($doneItems / $maxItems);
        if ($doneItems % $maxItems !== 0) {
            $idxFilename++;
        }
        if ($idxFilename > 1) {
            $this->exportFormat->cDateiname = $idxFilename . '_' . $this->exportFormat->cDateiname;
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->finished;
    }

    /**
     * @param int $estimatedCount
     * @return array
     */
    private function getExportProductIDs(int &$estimatedCount): array
    {
        $result = [];
        $sql    = $this->getExportSQL();

        if (!empty($sql)) {
            $db  = Shop::Container()->getDB();
            $sql = \str_replace('SELECT', 'SELECT SQL_CALC_FOUND_ROWS', $sql);

            $result         = $db->query($sql, ReturnType::ARRAY_OF_ASSOC_ARRAYS);
            $estimatedCount = (int)$db->query('SELECT FOUND_ROWS() AS count', ReturnType::SINGLE_OBJECT)->count;
        }

        return $result;
    }

    /**
     * @return void
     */
    private function writeZipFile(): void
    {
        if (\file_exists(self::EXPORT_PATH . $this->exportFormat->cDateiname)) {
            \unlink(self::EXPORT_PATH . $this->exportFormat->cDateiname);
        }
        $zipArchive = new \ZipArchive;
        if ($zipArchive->open(
            self::EXPORT_PATH . $this->exportFormat->cDateiname,
            (\is_file(self::EXPORT_PATH . $this->exportFormat->cDateiname) ? null : \ZipArchive::CREATE)
        ) === true) {
            $zipArchive->addFile(self::EXPORT_PATH . $this->tmpFilename, $this->tmpFilename);
            $zipArchive->close();
        }

        \unlink(self::EXPORT_PATH . $this->tmpFilename);
    }

    /**
     * @return void
     */
    private function start(): void
    {
        if ($this->queueEntry->tasksExecuted === 0 && \file_exists(self::EXPORT_PATH . $this->tmpFilename)) {
            \unlink(self::EXPORT_PATH . $this->tmpFilename);
        }

        $this->f = \fopen(self::EXPORT_PATH . $this->tmpFilename, 'a');
    }

    /**
     * @return void
     */
    private function stop(): void
    {
        if (\is_resource($this->f)) {
            \fclose($this->f);
        }

        if (!$this->isCron && !$this->finished) {
            $db = Shop::Container()->getDB();
            $db->queryPrepared(
                'UPDATE texportqueue SET
                    nLimit_n       = nLimit_n + :nLimitM,
                    nLastArticleID = :nLastArticleID
                    WHERE kExportqueue = :kExportqueue',
                [
                    'nLimitM'        => $this->queueEntry->taskLimit,
                    'nLastArticleID' => $this->queueEntry->lastProductID,
                    'kExportqueue'   => (int)$this->queueEntry->jobQueueID,
                ],
                ReturnType::DEFAULT
            );
            $cURL = Shop::getURL() . '/' . \PFAD_ADMIN . 'do_export.php?e=' . $this->queueEntry->jobQueueID
                . '&back=admin'
                . '&token=' . $_SESSION['jtl_token'];
            \header('Location: ' . $cURL);
            exit;
        }
    }

    private function finish(): void
    {
        $this->finished = true;
        try {
            $this->exportFormat->dZuletztErstellt = (new DateTime())->format('Y-m-d H:i:s');
        } catch (Exception $e) {
            $this->exportFormat->dZuletztErstellt = '';
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    private function doExport(): bool
    {
        $this->start();

        $estimatedCount = 0;
        $productIDs     = $this->getExportProductIDs($estimatedCount);

        if ($estimatedCount === 0) {
            $this->stop();
            \unlink(self::EXPORT_PATH . $this->tmpFilename);

            return false;
        }

        $xml = new GoogleShoppingXML(
            $this->exportFormat,
            $this->f,
            $this->settings,
            Shop::Container()->getDB()
        );
        $xml->setLocalization($this->plugin->getLocalization())
            ->setExportProductIds($productIDs);
        $this->queueEntry->lastProductID = (int)\end($productIDs)['kArtikel'];
        unset($productIDs);

        if ($this->queueEntry->tasksExecuted === 0
            || $this->queueEntry->tasksExecuted % (int)$this->settings->get('maxItem') === 0
        ) {
            $xml->writeHead();
        }
        $xml->writeContent();

        $this->queueEntry->tasksExecuted += \min($this->queueEntry->taskLimit, $estimatedCount);

        if ($this->queueEntry->taskLimit >= $estimatedCount) {
            $xml->writeFooter();
            $this->writeZipFile();
            $this->finish();
        }
        if ($this->queueEntry->tasksExecuted % $this->settings->get('maxItem') === 0) {
            $xml->writeFooter();
            $this->writeZipFile();
        }

        $this->stop();

        return true;
    }

    /**
     * @return string
     */
    public function getExportSQL(): string
    {
        return $this->exportSQL;
    }

    /**
     * @param string $sql
     * @return self
     */
    public function setExportSQL(string $sql): self
    {
        $this->exportSQL = $sql;

        return $this;
    }

    /**
     * @param object $exportFormat
     * @param array  $settings
     * @return self
     */
    public static function export(object $exportFormat, array $settings): self
    {
        return new static($exportFormat, $settings);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool
    {
        return $this->doExport();
    }
}
