<?php declare(strict_types=1);

namespace Plugin\jtl_google_shopping\src\Backend;

use JTL\Helpers\Text;
use JTL\Plugin\PluginInterface;
use JTL\Shop;

/**
 * Class CustomLink
 * @package Plugin\jtl_google_shopping\helpers
 */
abstract class CustomLink
{
    /** @var PluginInterface */
    private $plugin;

    /** @var object */
    private $menu;

    /** @var array */
    private $requestData;

    /**
     * CustomLink constructor.
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     */
    private function __construct(PluginInterface $plugin, object $menu, array $requestData = [])
    {
        $this->plugin      = $plugin;
        $this->menu        = $menu;
        $this->requestData = Text::filterXSS($requestData);
    }

    /**
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     * @return static
     */
    public static function handleRequest(PluginInterface $plugin, object $menu, array $requestData = []): self
    {
        $instance = new static($plugin, $menu, $requestData);
        if ($instance->controller()) {
            return $instance->display();
        }

        return $instance;
    }

    /**
     * @return bool
     */
    abstract protected function controller(): bool;

    /**
     * @return string
     */
    abstract protected function getTemplate(): string;

    /**
     * @return PluginInterface
     */
    public function getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @return object
     */
    public function getMenu(): object
    {
        return $this->menu;
    }

    /**
     * @return array
     */
    public function getRequestData(): array
    {
        return $this->requestData;
    }

    /**
     * @param array $requestData
     */
    public function setRequestData(array $requestData): void
    {
        $this->requestData = $requestData;
    }

    /**
     * @param string     $value
     * @param mixed|null $default
     * @return mixed
     */
    public function getRequestValue(string $value, $default = null)
    {
        $data = $this->getRequestData();

        return \array_key_exists($value, $data) ? $data[$value] : $default;
    }

    /**
     * @return $this
     */
    public function display(): self
    {
        try {
            Shop::Smarty()
                ->assign('requestData', $this->getRequestData())
                ->assign('kPlugin', $this->getPlugin()->getID())
                ->assign('kPluginAdminMenu', $this->getMenu()->kPluginAdminMenu)
                ->display($this->getPlugin()->getPaths()->getAdminPath() . $this->getTemplate());
        } catch (\SmartyException $e) {
            echo $e->getMessage();
        }

        return $this;
    }
}
