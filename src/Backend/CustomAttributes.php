<?php declare(strict_types=1);

namespace Plugin\jtl_google_shopping\src\Backend;

use JTL\Alert\Alert;
use JTL\DB\ReturnType;
use JTL\Helpers\Form;
use JTL\Shop;

/**
 * Class CustomAttributes
 * @package Plugin\jtl_google_shopping\src\Backend
 */
class CustomAttributes extends CustomLink
{
    /** @var string */
    private $stepPlugin = 'attribute';

    /**
     * @return bool
     */
    protected function controller(): bool
    {
        Shop::Container()->getGetText()->loadPluginLocale('custom_attributes', $this->getPlugin());
        $stepPlugin = $this->getRequestValue('stepPlugin');
        if (($stepPlugin === 'neuesAttr' || $stepPlugin === 'alteAttr') && Form::validateToken()) {
            if ($this->getRequestValue('btn_delete') !== null) {
                $this->delete();
            } elseif ($this->getRequestValue('btn_standard') !== null) {
                $this->reset();
            } elseif ($this->getRequestValue('btn_reset_all') !== null) {
                $this->resetAll();
            } elseif ($stepPlugin === 'neuesAttr') {
                $this->insert();
            } elseif ($stepPlugin === 'alteAttr') {
                $this->update();
            }
        }

        $this->show();

        return true;
    }

    /**
     * @return string
     */
    protected function getTemplate(): string
    {
        return 'templates/custom_attributes.tpl';
    }

    /**
     * @return array
     */
    private function getAttributeSrc(): array
    {
        return [
            \__('Article property')   => 'ArtikelEigenschaft',
            \__('Function attribute') => 'FunktionsAttribut',
            \__('Attribute')          => 'Attribut',
            \__('Feature')            => 'Merkmal',
            \__('Static value')       => 'WertName',
            \__('Parent attribute')   => 'VaterAttribut'
        ];
    }

    /**
     * @param object $data
     * @return bool
     */
    private function validate(object $data): bool
    {
        $alerts = Shop::Container()->getAlertService();
        $result = true;

        if ($data->bStandard === 0 && (!isset($data->cGoogleName) || \mb_strlen($data->cGoogleName) === 0)) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('A valid value for this must be entered', \__('Google name'))),
                'attribute_validate_GoogleName'
            );
            $result = false;
        }

        if (isset($data->eWertHerkunft) && $data->eWertHerkunft !== 'VaterAttribut'
            && (!isset($data->cWertName) || \mb_strlen($data->cWertName) === 0)
        ) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('A valid value for this must be entered', \__('Value name'))),
                'attribute_validate_WertName'
            );
            $result = false;
        }
        if (!isset($data->eWertHerkunft) || !\in_array($data->eWertHerkunft, $this->getAttributeSrc(), true)) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('A valid value for this must be entered', \__('Value type'))),
                'attribute_validate_Werttyp'
            );
            $result = false;
        }
        if (($data->kVaterAttribut ?? 0) > 0) {
            $attribute = Shop::Container()->getDB()->select(
                'xplugin_jtl_google_shopping_attribut',
                'kAttribut',
                $data->kVaterAttribut
            );

            if ($attribute->eWertHerkunft !== 'VaterAttribut') {
                $alerts->addAlert(
                    Alert::TYPE_ERROR,
                    \__('Only IDs are allowed for which the value type Parent attribute is selected'),
                    'attribute_validate_VaterAttribut'
                );
                $result = false;
            }
            if ($data->eWertHerkunft === 'VaterAttribut') {
                $alerts->addAlert(
                    Alert::TYPE_ERROR,
                    \__('Leave this blank, if this attribute is not a child attribute'),
                    'attribute_validate_VaterAttribut'
                );
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @return void
     */
    private function delete(): void
    {
        $db          = Shop::Container()->getDB();
        $attributeID = (int)\key($this->getRequestValue('btn_delete'));
        $affected    = $db->queryPrepared(
            'DELETE FROM xplugin_jtl_google_shopping_attribut
                WHERE bStandard != 1
                    AND kAttribut = :attributeID',
            [
                'attributeID' => $attributeID
            ],
            ReturnType::AFFECTED_ROWS
        );
        if ($affected < 0) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Attribute with ID could not be ...'), $attributeID, \__('deleted')),
                'attribute_delete'
            );
        } elseif ($affected > 0) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \sprintf(\__('Attribute with ID successfully ...'), $attributeID, \__('deleted')),
                'attribute_delete'
            );
        }

        $this->setRequestData([]);
    }

    /**
     * @return void
     */
    private function reset(): void
    {
        $db          = Shop::Container()->getDB();
        $attributeID = (int)\key($this->getRequestValue('btn_standard'));
        $affected    = $db->queryPrepared(
            'UPDATE xplugin_jtl_google_shopping_attribut
                SET
                    cGoogleName    = cStandardGoogleName,
                    cWertName      = cStandardWertName,
                    eWertHerkunft  = eStandardWertHerkunft,
                    kVaterAttribut = kStandardVaterAttribut
                WHERE bStandard = 1
                    AND kAttribut = :attributeID',
            [
                'attributeID' => $attributeID
            ],
            ReturnType::AFFECTED_ROWS
        );
        if ($affected < 0) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Attribute with ID could not be ...'), $attributeID, \__('reset')),
                'attribute_reset'
            );
        } elseif ($affected > 0) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \sprintf(\__('Attribute with ID successfully ...'), $attributeID, \__('reset')),
                'attribute_reset'
            );
        }

        $this->setRequestData([]);
    }

    /**
     * @return void
     */
    private function resetAll(): void
    {
        $db = Shop::Container()->getDB();
        $db->query(
            'TRUNCATE TABLE xplugin_jtl_google_shopping_attribut',
            ReturnType::DEFAULT
        );

        $installer = new Installer($this->getPlugin(), $db);
        if ($installer->installAttributeData()) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \__('All attributes would be successfully reset to default values'),
                'attribute_reset_all'
            );
        } else {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                \__('Attributes could not be reset to default values'),
                'attribute_reset_all'
            );
        }
    }

    /**
     * @return void
     */
    private function insert(): void
    {
        $attribute                 = new \stdClass();
        $attribute->cGoogleName    = \trim($this->getRequestValue('cGoogleName'));
        $attribute->cWertName      = \trim($this->getRequestValue('cWertName'));
        $attribute->eWertHerkunft  = \trim($this->getRequestValue('eWertHerkunft'));
        $attribute->kVaterAttribut = (int)$this->getRequestValue('kVaterAttribut', 0);
        $attribute->bAktiv         = $this->getRequestValue('bAktiv') !== null ? 1 : 0;
        $attribute->bStandard      = 0;

        if ($this->validate($attribute)) {
            if (Shop::Container()->getDB()->insert('xplugin_jtl_google_shopping_attribut', $attribute) > 0) {
                $this->setRequestData([]);
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_SUCCESS,
                    \__('Attribute would be successfully added'),
                    'attribute_insert'
                );
            } else {
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_ERROR,
                    \__('Attribute could not be added'),
                    'attribute_insert'
                );
            }
        }
    }

    /**
     * @return void
     */
    private function update(): void
    {
        $db           = Shop::Container()->getDB();
        $attributeSrc = $this->getRequestValue('eWertHerkunft');

        if (\is_array($attributeSrc)) {
            foreach ($attributeSrc as $key => $value) {
                $attribute       = new \stdClass();
                $googleName      = \trim($this->getRequestValue('cGoogleName', [$key => ''])[$key] ?? '');
                $updateKeys      = ['kAttribut'];
                $updateKeyValues = [(int)$key];

                if (\strlen($googleName) > 0) {
                    $attribute->cGoogleName    = $googleName;
                    $attribute->kVaterAttribut = (int)$this->getRequestValue('kVaterAttribut', [$key => '0'])[$key];
                    $attribute->bStandard      = 0;
                } else {
                    $attribute->bStandard = 1;
                    $updateKeys[]         = 'bStandard';
                    $updateKeyValues[]    = 1;
                }

                $attribute->cWertName     = \trim($this->getRequestValue('cWertName', [$key => ''])[$key]);
                $attribute->eWertHerkunft = \trim($this->getRequestValue('eWertHerkunft', [$key => ''])[$key]);
                $attribute->bAktiv        = ($this->getRequestValue('bAktiv', [$key => null])[$key] ?? null) !== null
                    ? 1
                    : 0;

                if ($this->validate($attribute)) {
                    $affected = $db->update(
                        'xplugin_jtl_google_shopping_attribut',
                        $updateKeys,
                        $updateKeyValues,
                        $attribute
                    );

                    if ($affected < 0) {
                        Shop::Container()->getAlertService()->addAlert(
                            Alert::TYPE_ERROR,
                            \sprintf(\__('Attribute with ID could not be ...'), $key, \__('changed')),
                            'attribute_update_' . $key
                        );
                    } elseif ($affected > 0) {
                        Shop::Container()->getAlertService()->addAlert(
                            Alert::TYPE_SUCCESS,
                            \sprintf(\__('Attribute with ID successfully ...'), $key, \__('changed')),
                            'attribute_update_' . $key
                        );
                    }
                }
            }
        }

        $this->setRequestData([]);
    }

    /**
     * @return void
     */
    private function show(): void
    {
        $db                 = Shop::Container()->getDB();
        $allChildAttributes = [];
        $attributes         = $db->query(
            'SELECT * FROM xplugin_jtl_google_shopping_attribut
                WHERE kVaterAttribut = 0
                ORDER BY kAttribut',
            ReturnType::ARRAY_OF_OBJECTS
        );
        $childAttributes    = $db->query(
            'SELECT * FROM xplugin_jtl_google_shopping_attribut
                WHERE kVaterAttribut != 0
                ORDER BY kAttribut',
            ReturnType::ARRAY_OF_OBJECTS
        );

        foreach ($childAttributes as $childAttribute) {
            if (isset($allChildAttributes[$childAttribute->kVaterAttribut])
                && \is_array($allChildAttributes[$childAttribute->kVaterAttribut])
            ) {
                $allChildAttributes[$childAttribute->kVaterAttribut][] = $childAttribute;
            } else {
                $allChildAttributes[$childAttribute->kVaterAttribut] = [$childAttribute];
            }
        }

        Shop::Smarty()
            ->assign('attribute_arr', $attributes)
            ->assign('kindAttribute_arr', $allChildAttributes)
            ->assign('eWertHerkunft_arr', $this->getAttributeSrc())
            ->assign('stepPlugin', $this->stepPlugin);
    }
}
