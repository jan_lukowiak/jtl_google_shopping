<?php declare(strict_types=1);

namespace Plugin\jtl_google_shopping\src\Backend;

use JTL\Alert\Alert;
use JTL\DB\ReturnType;
use JTL\Helpers\Form;
use JTL\Shop;
use stdClass;

/**
 * Class Lang
 * @package Plugin\jtl_google_shopping\helpers
 */
class CustomExports extends CustomLink
{
    /** @var string */
    private $stepPlugin = 'sprachen';

    /**
     * @return bool
     */
    protected function controller(): bool
    {
        Shop::Container()->getGetText()->loadPluginLocale('custom_exports', $this->getPlugin());
        $stepPlugin = $this->getRequestValue('stepPlugin');
        if ($stepPlugin === $this->stepPlugin && Form::validateToken()) {
            if ($this->getRequestValue('btn_save_new', '') !== '') {
                $this->insert();
            } elseif ($this->getRequestValue('btn_delete', '') !== '') {
                $this->delete();
            }
        }

        $this->show();

        return true;
    }

    /**
     * @return string
     */
    protected function getTemplate(): string
    {
        return 'templates/custom_exports.tpl';
    }

    /**
     * @param object $data
     * @return bool
     */
    private function validate(object $data): bool
    {
        $alerts = Shop::Container()->getAlertService();
        $result = true;

        if (empty($data->cName)) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Field can not be empty'), \__('Export name')),
                'lang_validate_name'
            );

            $result = false;
        }

        if (empty($data->cDateiname)) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Field can not be empty'), \__('File name')),
                'lang_validate_filename'
            );

            $result = false;
        }

        if (empty($data->kSprache)) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Field can not be empty'), \__('Language')),
                'lang_validate_language'
            );

            $result = false;
        }

        if (empty($data->kKundengruppe)) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Field can not be empty'), \__('Customer group')),
                'lang_validate_customergroup'
            );

            $result = false;
        }

        if (empty($data->kWaehrung)) {
            $alerts->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Field can not be empty'), \__('Currency')),
                'lang_validate_currency'
            );

            $result = false;
        }

        return $result;
    }

    /**
     * @return void
     */
    private function insert(): void
    {
        $db                    = Shop::Container()->getDB();
        $export                = new stdClass();
        $export->cName         = \trim($this->getRequestValue('cName', ''));
        $export->cDateiname    = \trim($this->getRequestValue('cDateiname', ''));
        $export->kSprache      = (int)$this->getRequestValue('kSprache', 0);
        $export->kKundengruppe = (int)$this->getRequestValue('kKundengruppe', 0);
        $export->kWaehrung     = (int)$this->getRequestValue('kWaehrung', 0);
        $export->kPlugin       = $this->getPlugin()->getID();
        $export->cKopfzeile    = ' ';
        $export->cContent      = 'PluginContentFile_googleShopping.php';
        $export->cFusszeile    = ' ';

        if ($this->validate($export)) {
            $insertedID = $db->insert('texportformat', $export);

            if ($insertedID > 0) {
                $configValues = $db->selectAll(
                    'teinstellungenconf',
                    'kEinstellungenSektion',
                    \CONF_EXPORTFORMATE,
                    'cWertName'
                );

                foreach ($configValues as $configValue) {
                    $formatSettings                = new stdClass();
                    $formatSettings->kExportformat = $insertedID;
                    $formatSettings->cName         = $configValue->cWertName;

                    if ($configValue->cWertName === 'exportformate_lieferland') {
                        $formatSettings->cWert = \trim($this->getRequestValue('cLieferlandIso', ''));
                    } else {
                        $formatSettings->cWert = 'N';
                    }

                    $db->insert('texportformateinstellungen', $formatSettings);
                }

                $this->setRequestData([]);
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_SUCCESS,
                    \sprintf(\__('Export format would be successfully...'), \__('created')),
                    'export_insert'
                );
            } else {
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_ERROR,
                    \sprintf(\__('Export format could not be...'), \__('created')),
                    'export_insert'
                );
            }
        }
    }

    /**
     * @return void
     */
    private function delete(): void
    {
        $db       = Shop::Container()->getDB();
        $ExportID = (int)$this->getRequestValue('btn_delete');

        if ($ExportID > 0) {
            $db->delete('texportformateinstellungen', 'kExportformat', $ExportID);
            $db->delete('texportformat', 'kExportformat', $ExportID);

            $this->setRequestData([]);
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_SUCCESS,
                \sprintf(\__('Export format would be successfully...'), \__('deleted')),
                'export_delete'
            );
        } else {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                \sprintf(\__('Export format could not be...'), \__('deleted')),
                'export_delete'
            );
        }
    }

    /**
     * @return void
     */
    private function show(): void
    {
        $db                = Shop::Container()->getDB();
        $languages         = $db->selectAll('tsprache', [], [], 'kSprache, cNameDeutsch', 'cNameDeutsch');
        $customerGroups    = $db->selectAll('tkundengruppe', [], [], 'kKundengruppe, cName', 'cName');
        $currencies        = $db->selectAll('twaehrung', [], [], 'kWaehrung, cName', 'cName');
        $deliveryMethods   = $db->selectAll('tversandart', [], [], 'cLaender');
        $shippingCountries = '';
        foreach ($deliveryMethods as $deliveryMethod) {
            $shippingCountries .= ' ' . $deliveryMethod->cLaender;
        }
        $shippingCountries = \array_unique(\explode(' ', $shippingCountries));
        \sort($shippingCountries);

        $exportFormats = $db->queryPrepared(
            "SELECT
                texportformat.kExportformat,
                texportformat.cName,
                texportformat.cDateiname,
                texportformateinstellungen.cWert AS cLieferlandIso,
                tsprache.cNameDeutsch AS cSprache,
                tkundengruppe.cName AS cKundengruppe,
                twaehrung.cName AS cWaehrung
                FROM texportformat
                LEFT JOIN texportformateinstellungen
                    ON texportformat.kExportformat = texportformateinstellungen.kExportformat
                        AND texportformateinstellungen.cName = 'exportformate_lieferland'
                LEFT JOIN tsprache
                    ON texportformat.kSprache = tsprache.kSprache
                LEFT JOIN tkundengruppe
                    ON texportformat.kKundengruppe = tkundengruppe.kKundengruppe
                LEFT JOIN twaehrung
                    ON texportformat.kWaehrung = twaehrung.kWaehrung
                WHERE kPlugin = :pluginID",
            ['pluginID' => $this->getPlugin()->getID()],
            ReturnType::ARRAY_OF_OBJECTS
        );

        Shop::Smarty()
            ->assign('oExportformate', $exportFormats)
            ->assign('oSprache_arr', $languages)
            ->assign('oKundengruppen_arr', $customerGroups)
            ->assign('oWaehrung_arr', $currencies)
            ->assign('cVersandlandIso_arr', $shippingCountries)
            ->assign('stepPlugin', $this->stepPlugin);
    }
}
