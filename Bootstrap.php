<?php declare(strict_types = 1);

namespace Plugin\jtl_google_shopping;

use JTL\Backend\NotificationEntry;
use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use Plugin\jtl_google_shopping\src\Backend\Installer;

/**
 * Class Bootstrap
 * @package Plugin\jtl_google_shopping
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @inheritDoc
     */
    public function installed()
    {
        parent::installed();

        $installer = new Installer($this->getPlugin(), $this->getDB());
        $installer->install();
    }

    /**
     * @inheritDoc
     */
    public function uninstalled(bool $deleteData = true)
    {
        if ($deleteData) {
            $installer = new Installer($this->getPlugin(), $this->getDB());
            $installer->uninstall();
        }

        parent::uninstalled($deleteData);
    }
}
