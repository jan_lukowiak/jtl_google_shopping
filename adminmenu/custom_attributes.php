<?php declare(strict_types=1);

use Plugin\jtl_google_shopping\src\Backend\CustomAttributes;

global $plugin;

/** @var object $menu */
CustomAttributes::handleRequest($plugin, $menu, $_POST);
