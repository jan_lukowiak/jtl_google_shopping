<?php declare(strict_types=1);

use Plugin\jtl_google_shopping\src\Backend\CustomExports;

global $plugin;

/** @var object $menu */
CustomExports::handleRequest($plugin, $menu, $_POST);
