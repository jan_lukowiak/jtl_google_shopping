<?php declare(strict_types=1);

use Plugin\jtl_google_shopping\src\Backend\CustomMapping;

global $plugin;

/** @var object $menu */
CustomMapping::handleRequest($plugin, $menu, $_POST);
